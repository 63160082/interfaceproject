/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.interfaceproject;

/**
 *
 * @author ACER
 */
public class Cat extends LandAnimal {

    public Cat(String name) {
        super("Cat", 4);
    }

    @Override
    public void eat() {
        System.out.println("Cat : Eat");
    }

    @Override
    public void speak() {
        System.out.println("Cat : Speak");
    }

    @Override
    public void sleep() {
        System.out.println("Cat : Sleep");
    }

    @Override
    public void run() {
        System.out.println("Cat : Run");
    }
    
}
