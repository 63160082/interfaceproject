/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.interfaceproject;

/**
 *
 * @author ACER
 */
public class Bat extends Poultry {
    private String nickname;
    
    public Bat(String nickname) {
        super();
        this.nickname = nickname;
    }

    @Override
    public void eat() {
       System.out.println("Bat : Eat");        
    }

    @Override
    public void speak() {
       System.out.println("Bat : Speak");        
    }

    @Override
    public void sleep() {
       System.out.println("Bat : Sleep");       
    }

    @Override
    public void fly() {
        System.out.println("Bat : Fly");
    }
    
}
