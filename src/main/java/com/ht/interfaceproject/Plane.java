/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.interfaceproject;

/**
 *
 * @author ACER
 */
public class Plane extends Vehicle implements Flyable,Runable {

    public Plane(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Plane : Start Engine");
    }

    @Override
    public void stopEngine() {
        System.out.println("Plane : End Engine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Plane : Raise Speed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Plane : Apply Break");
    }

    @Override
    public void fly() {
        System.out.println("Plane : Fly");
    }

    @Override
    public void run() {
        System.out.println("Plane : Run");
    }
    
}
