/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.interfaceproject;

/**
 *
 * @author ACER
 */
public class Test {
    public static void main(String[] args) {
        Bat bat = new Bat("Bat");
        Plane plane = new Plane("Engine no. 1");
        bat.fly();
        plane.fly();
        
        Bird bird = new Bird("Bird");
        
        Flyable [] flyables = {bat,plane,bird};
        for(Flyable f : flyables) {
            if(f instanceof Plane) {
                Plane p = (Plane)f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }
        
        Dog dog = new Dog("Dog");
        Cat cat = new Cat("Meow");
        Human human = new Human("Human");
        Car car = new Car("Engine no. 2");
        car.run();
        
        Runable [] runables = {dog,plane,cat,human,car};
        for(Runable r : runables) {
            r.run();
        }
    }
}
