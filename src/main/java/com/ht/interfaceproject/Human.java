/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.interfaceproject;

/**
 *
 * @author ACER
 */
public class Human  extends LandAnimal{

    public Human(String name) {
        super("Human", 2);
    }

    @Override
    public void eat() {
        System.out.println("Human : Eat");
    }

    @Override
    public void speak() {
        System.out.println("Human : Speak");
    }

    @Override
    public void sleep() {
        System.out.println("Human : Sleep");
    }

    @Override
    public void run() {
        System.out.println("Human : Run");
    }
    
}
