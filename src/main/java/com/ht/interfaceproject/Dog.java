/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.interfaceproject;

/**
 *
 * @author ACER
 */
public class Dog extends LandAnimal {

    public Dog(String name) {
        super("Dog",4);
    }

    @Override
    public void eat() {
        System.out.println("Dog : Eat");
    }

    @Override
    public void speak() {
        System.out.println("Dog : Speak");
    }

    @Override
    public void sleep() {
        System.out.println("Dog : Sleep");
    }

    @Override
    public void run() {
        System.out.println("Dog : Run");
    }
    
}
