/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.interfaceproject;

/**
 *
 * @author ACER
 */
public class Bird extends Poultry {
    
    public Bird(String name) {
        super();
    }
   
    @Override
    public void eat() {
        System.out.println("Bird : Eat");
    }

    @Override
    public void speak() {
        System.out.println("Bird : Speak");
    }

    @Override
    public void sleep() {
        System.out.println("Bird : Sleep");
    }

    @Override
    public void fly() {
        System.out.println("Bird : Fly");
    }
    
}
